#include<stdio.h>
#define size 100
//Program to add and multiply two given matrix

int x[size][size];//Array x
int y[size][size];//Array y
int add[size][size];//Array add
int mul[size][size];//Array mul
int xRows,xCol,yRows,yCol;
int a,b,c,sum=0;

int main()
{
    //Matrix x
    printf("Enter the number of Rows and Columns of Matrix x :");//Enter the row and columns of Matrix x
    scanf("%d %d",&xRows, &xCol);
    printf("Rows:%d Columns:%d\n",xRows,xCol);

    printf("Enter the Elements of Matrix x :\n");//Enter the Elements of Matrix x
    for(a=0; a<xRows; a++)
    {
        for(b=0; b<xCol; b++)
             scanf("%d",&x[a][b]);
    }
    printf("\n");


    //Matrix y
    printf("Enter the number of Rows and Columns of Matrix y :");//Enter the rows and columns of Matrix y
    scanf("%d %d",&yRows,&yCol);
    printf("Rows:%d Columns:%d\n",yRows,yCol);

    printf("Enter the Elements of Matrix y :\n");//Enter the Elements of Matrix y
    for(a=0; a<yRows; a++)
    {
        for(b=0; b<yCol; b++)
            scanf("%d",&y[a][b]);
    }
    printf("\n");

    //Checks posibility
    if(xRows!=yRows||xCol!=yCol)
    {
        printf("Matrix Multiplication is not possible\n");
        return;
    }
    if(xCol!=yRows)
    {
         printf("Matrix Multiplication is not possible\n");
        return;
    }
    else
    {
        //Matrix Addition
        for(a=0; a<xRows; a++)
            for(b=0; b<xCol; b++)
            add[a][b] = x[a][b] + y[a][b];
        printf("Matrix Addition :\n");
        for(a=0; a<xRows; a++)
        {
            for(b=0; b<xCol; b++)
                printf("%d\t",add[a][b]);
            printf("\n");
        }
        printf("\n");


        //Matrix Multiplication
        for(a=0; a<xRows; a++)
        {
            for(b=0; b<yCol; b++)
               {
                   for(c=0; c<yRows; c++)
                    mul[a][b] = mul[a][b] + x[a][c]*y[c][b];
               }
        }
         printf("Matrix Multiplication :\n");
    for(a=0; a<xRows; a++)
    {
        for(b=0; b<yCol; b++)
        printf("%d\t",mul[a][b]);
        printf("\n");
    }
    }


    return 0;
}
